import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import data from './data'
import SelectCurrency from './SelectCurrency'
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

class Board extends React.Component
{
	constructor(props) {
		super(props);

		this.state = {
			currencies : data.currencies,
			currencySrc : data.currencies[0],
			currencyDest : data.currencies[1],
			currencySrcVal : data.currencies[0].rate,
			currencyDestVal : data.currencies[1].rate
		}


		//console.log(this);
	 this.onSelectCurrency = this.onSelectCurrency.bind(this);
	 //	 this.onSelectSrcCurrency = this.onSelectSrcCurrency.bind(this);
	  //   this.handleChange = this.handleChange.bind(this);
	}
	componentDidMount() { 
		// Doing AJAX in componentDidMount will guarantee that there’s a component to update.
	 		var parseString = require('xml2js').parseString;
			var xml = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
			let xmlData;
			 const self = this;
			 
			fetch(xml)
			        .then(response => response.text())
			        .then(str => (new window.DOMParser()).parseFromString(str, "text/xml"))
			        .then(data => console.log(data))
			       .then(data => this.setState({ xmlData : data}))
   			        .catch((err) => {
			            console.log('fetch', err)
			        });	
	
	}	


 	
	  onSelectCurrency(code){
	    //console.log('selecting currency: '+code);
	    const {currencies, currencySrcVal} = this.state;
	    const currency = currencies.filter(currency => currency.currency === code);
	    this.setState({
		      currencyDest: currency[0], // this is an array with one item
		      currencyDestVal: currencySrcVal * currency[0].rate
	    })
	  }
	
	  onChangeHandler(e, currency){
	
	    const {currencySrc, currencyDest} = this.state;
	
	    if(currency === 'A'){	      
		      const newValueSrc = e.target.value;
		      this.setState({
		        currencySrcVal: newValueSrc,
		        currencyDestVal: newValueSrc * currencyDest.rate
		      })
	
	    } else if(currency === 'B'){
	      
		      const newValueDest = e.target.value;
		      this.setState({
		        currencySrcVal: newValueDest / currencyDest.rate,
		        currencyDestVal: newValueDest
		      })	
	    }
	  }
	  
		render()
	{

		const {currencies, currencySrc, currencyDest, currencySrcVal, currencyDestVal, xmlData } = this.state;
		console.log(this.state);

		return (
					
	      <div className="wrapper">
	        	<header>
					<h1>Currency Converter</h1>
				</header>
	        <div className="content">
				<div className="row row-select-currency">
					<div className="col-md-6 info-box">
						<div className="src-currency-info">1 USD=64 GBP</div>
					</div>	
					<div className="col-md-6">
						<div className="dest-currency-info">1 GBP=64 USD</div>
					</div>
				</div>
	    
				<div className="row">          
					<div className="col-sm-6 src-currency-info" >
						<div className="input-group src-currency-info">
					    	<span className="input-group-addon"></span>
							<div className="cur-dropdown source-dropdown"><SelectCurrency currencies={currencies} onSelectCurrency={this.onSelectCurrency} /></div>							
						</div>
					</div>
					<div className="col-sm-6 currency-to-input dest-currency-info">
						<div className="input-group">
					        <span className="input-group-addon"></span>
					        <div className="cur-dropdown dest-dropdown"><SelectCurrency currencies={currencies} onSelectCurrency={this.onSelectCurrency} /></div>
						</div>
					</div>            
	           </div>
	           <div className="row">
	
	            	<div className="col-sm-6 src-currency-input" >
	            		<input type="number" defaultValue={currencySrc} className="form-control" aria-describedby="basic-addon2" step="1" pattern="\d\.\d{2}"   onChange={(e) => { this.onChangeHandler(e, 'A'); } } />
					</div>              
					<div className="col-sm-6 dest-currency-input" >
	            
						<input type="number" value={currencyDestVal} className="form-control" aria-describedby="basic-addon3" step="1" pattern="\d\.\d{2}"  onChange={(e) => { this.onChangeHandler(e, 'B'); } } />
					</div>  
	
				</div>
	        </div>
	      </div>	
		)
	}
}


// ========================================

ReactDOM.render(
     <div><Board /></div>, document.getElementById('root')
);
