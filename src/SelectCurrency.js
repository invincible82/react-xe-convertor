import React from 'react';
import PropTypes from 'prop-types';

const SelectCurrency = ({currencies, onSelectCurrency}) => {

    const filteredCurrencies = currencies.filter(currency => currency.currency !== 'XYZ');

	console.log("filtered: "+ filteredCurrencies); 
	
    return <select className="dropdown" onChange={(e) => onSelectCurrency(e.target.value)}>
        {
            filteredCurrencies.map(currency => {
                
                return <option key={currency.currency} value={currency.currency}>{currency.currency}</option>
            })
        }
    </select>
}

SelectCurrency.propTypes = {
    currencies: PropTypes.array.isRequired,
    onSelectCurrency: PropTypes.func.isRequired
};

export default SelectCurrency;