const data = {
    "currencies": [
        {
            "currency": "AUD",
            "rate": 1,
        },
        {
            "currency": "USD",
            "rate": .7041,

        },
        {
            "currency": "CAD",
            "rate": .9504,

        },
        {
            "currency": "NZD",
            "rate": .9949,
        },
        {
            "currency": "JPY",
            "rate": 78.21,

        },
        {
            "currency": "GBP",
            "rate": .5472,
        }
    ]
}

export default data;